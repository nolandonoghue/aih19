from sklearn import tree
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
%matplotlib inline
from matplotlib import pyplot as plt
iris = datasets.load_iris()

X, y = iris.data, iris.target
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2)

dtree = tree.DecisionTreeClassifier(criterion="gini", min_samples_split=4, splitter="best")
dtree.fit(X_train,y_train)
test_pred = dtree.predict(X_test)
tree.plot_tree(dtree, filled=True)
plt.gcf().set_size_inches((5, 6))
print(accuracy_score(y_test, test_pred))
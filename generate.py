import numpy as np


def generate_dataset(train_rows, num_feats, test_rows, seed=0):
    np.random.seed(seed)
    train_points = 100 * np.random.rand(train_rows, num_feats).round(3)
    train_labels = np.where(
        np.sum(
            np.where(train_points > 50, 1, 0), axis=1
        )>(num_feats/2), True, False
    )
    rand_ind = np.random.randint(0, train_points.shape[0], size=(test_rows,))
    test_points = train_points[rand_ind]
    test_labels = train_labels[rand_ind]
    test_points = (test_points 
                   + (2*np.sqrt(num_feats))*np.random.randn(test_points.shape[0], test_points.shape[1])).round(3)
    return train_points, train_labels, test_points, test_labels